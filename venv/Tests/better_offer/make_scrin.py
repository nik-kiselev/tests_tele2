#Выводит скриншоты правил
from selenium import webdriver
import time
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

chrome_options = webdriver.ChromeOptions()
chrome_options.add_argument("--disable-notifications")
driver = webdriver.Chrome(chrome_options=chrome_options)
driver.get("https://msk.tele2.ru/")

time.sleep(7)
element = WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH, '//*[@id="fl-171881"]')))
driver.switch_to_frame(element)

element = WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.CSS_SELECTOR, 'button[class="intro__button no-outline"]')))
element.click()

#element = driver.find_element_by_css_selector('a[class="js-rules"]')
#element.click()
current_url = driver.current_url
#new_window_url = driver.find_element_by_css_selector('a[class="js-rules"]')
new_window_url = driver.find_element_by_css_selector('a[class="js-rules"]')
new_window_url.click()
new_window_personal = driver.find_element_by_css_selector('a[class="js-personal"]')
new_window_personal.click()
#driver.get(new_window_url)
window_before = driver.window_handles[0]
time.sleep(2)

window_rules = driver.window_handles[1]
driver.switch_to_window(window_rules)
time.sleep(7)
driver.save_screenshot('rules.png')

window_personal = driver.window_handles[2]
driver.switch_to_window(window_personal)
time.sleep(7)
driver.save_screenshot('personal.png')
driver.quit()