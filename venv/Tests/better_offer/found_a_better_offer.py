from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

chrome_options = webdriver.ChromeOptions()
chrome_options.add_argument("--disable-notifications")
driver = webdriver.Chrome(chrome_options=chrome_options)
driver.get("https://msk.tele2.ru/")

element = WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH, '//*[@id="fl-171881"]')))
driver.switch_to_frame(element)

element = driver.find_element_by_css_selector('button[class="intro__button no-outline"]')
element.click()
element = driver.find_element_by_name('user_phone')
element.send_keys('+78888888888')
element = driver.find_element_by_name('user_name')
element.send_keys('Test name')
element = driver.find_element_by_css_selector('a[class="close js-to-intro"]')
element.click()


#search_field.click()
