#Проверка наличия стика у тарифа
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
import time

chrome_options = webdriver.ChromeOptions()
chrome_options.add_argument("--disable-notifications")
driver = webdriver.Chrome(chrome_options=chrome_options)
driver.maximize_window()
driver.get("https://msk.tele2.ru/")
time.sleep(5)

elements = driver.find_elements_by_css_selector('div[class="ssc-tariff-box squeezed"]')
for element in elements:
    a = element.find_element_by_css_selector('div.tariff-title > span > span').text
    try:
        element.find_element_by_css_selector('img[class="hit-image"]')
        print('Тариф %s имеет стик Хит продаж' % a)
    except Exception:
        print('Тариф %s не имеет стик Хит продаж' % a)

