#Выводит в консоль url картинок на странице
from selenium import webdriver
from selenium.webdriver.common.keys import Keys

chrome_options = webdriver.ChromeOptions()
chrome_options.add_argument("--disable-notifications")
driver = webdriver.Chrome(chrome_options=chrome_options)
driver.maximize_window()
driver.get(" https://more.tele2.ru/")

for elements in driver.find_elements_by_css_selector('img'):
    print(elements.get_attribute("src"))