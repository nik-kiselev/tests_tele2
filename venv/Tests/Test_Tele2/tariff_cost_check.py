#Проверка цены тарифа
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time

chrome_options = webdriver.ChromeOptions()
chrome_options.add_argument("--disable-notifications")
driver = webdriver.Chrome(chrome_options=chrome_options)
driver.maximize_window()
driver.get("https://msk.tele2.ru/")

time.sleep(5)
price = driver.find_element_by_xpath('//*[@id="root"]/div/div[1]/div/div/div/div/div[2]/div[2]/div/div/div/div/div[2]/div/div[1]/div[3]/div/span')
price.click()
a = price.text

time.sleep(5)
price_interior = driver.find_element_by_xpath('//*[@id="root"]/div/div[1]/div/div/div/div/div[2]/div/div/div[2]/div/div[4]/div/div[1]/div/span/span')
if a == price_interior.text:
    print('Стоимость тарифа на главной равна стоимости во внутренней странице тарифа')
else:
    print('Стоимость тарифа на главной не равна стоимости во внутренней странице тарифа')
driver.quit()