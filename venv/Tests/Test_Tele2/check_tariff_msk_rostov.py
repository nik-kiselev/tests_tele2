#Проверка цены тарифа Москвы и Ростова
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time

chrome_options = webdriver.ChromeOptions()
chrome_options.add_argument("--disable-notifications")
driver = webdriver.Chrome(chrome_options=chrome_options)
driver.maximize_window()
driver.get("https://msk.tele2.ru/")

time.sleep(5)
price = driver.find_element_by_xpath('//*[@id="root"]/div/div[1]/div/div/div/div/div[2]/div[2]/div/div/div/div/div[2]/div/div[1]/div[3]/div/span')
a = price.text

driver.get("https://rostov.tele2.ru/")
time.sleep(5)
price_interior = driver.find_element_by_xpath('//*[@id="root"]/div/div[1]/div/div/div/div/div[2]/div[2]/div/div/div/div/div[2]/div/div[1]/div[3]/div/span')
if a == price_interior.text:
    print('Стоимость тарифа на msk равна стоимости на rostov')
else:
    print('Стоимость тарифа на msk не равна стоимости на rostov')
driver.quit()

