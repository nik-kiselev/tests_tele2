#Логинится в ЛК и выводит остатки. Данные вводить без маски
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

print('Введите телефон:')
a = input()
print('Введите пароль:')
b = input()
print('Ваши данные:'+' '+a + ' '+b)
a = '9514900295'
b = '887805'

chrome_options = webdriver.ChromeOptions()
chrome_options.add_argument("--disable-notifications")
driver = webdriver.Chrome(chrome_options=chrome_options)
driver.maximize_window()

driver.get("https://msk.tele2.ru/")

wait = WebDriverWait(driver, 15)

search_field_lk =driver.find_element_by_xpath('//*[@id="root"]/div/div[1]/div/div/div/div/div[1]/div/div/div/div[2]/div/div/div/div[3]/div[1]/div/a')
search_field_lk.click()

search_field_login_pass = WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH, '//*[@id="loginDialog"]/div/div[2]/iframe')))
driver.switch_to_frame(search_field_login_pass)

search_field_login_pass = WebDriverWait(driver, 20).until(EC.presence_of_element_located((By.XPATH, '//*[@id="card-auth"]/div/div/ul/li[2]/a')))
#search_field_login_pass =  WebDriverWait(driver, 20).until(EC.presence_of_element_located((By.XPATH, '//*[@id="card-auth"]/div/div/ul/li[2]/a')))
search_field_login_pass.click()

search_field_login_pass = WebDriverWait(driver, 25).until(EC.presence_of_element_located((By.ID, 'phone-password')))
search_field_login_pass.send_keys(a)

search_field_login_pass = driver.find_element_by_id('password-field')
search_field_login_pass.send_keys(b)

search_field_login_pass = driver.find_element_by_xpath('//*[@id="password-form"]/div[4]/div[1]/input')
search_field_login_pass.click()

#Заходим в ЛК
#search_field_login_pass = WebDriverWait(driver, 20).until(EC.presence_of_element_located((By.XPATH, '//*[@id="root"]/div/div[1]/div/div/div/div/div[1]/div/div/div/div[2]/div/div/div/div[2]/div[1]/div/a')))
#search_field_login_pass.click()

#balance = WebDriverWait(driver, 20).until(EC.presence_of_element_located((By.XPATH, '//*[@id="root"]/div/div[1]/div/div/div/div/div[1]/div/div/div/div[2]/div/div/div/div[3]/div[1]/div/div[2]/div/div/div/div[1]/div[2]/div[1]/div/span[1]')))
balancewait = WebDriverWait(driver, 20).until(EC.presence_of_element_located((By.XPATH, '//*[@id="root"]/div/div[1]/div/div/div/div/div[1]/div/div/div/div[2]/div/div/div/div[3]/div[1]/div/div[2]/div/div/div/div[1]/div[2]/div[1]/div/span[1]')))
#balance = wait.until(lambda driver: driver.find_element_by_xpath('//*[@id="root"]/div/div[1]/div/div/div/div/div[1]/div/div/div/div[2]/div/div/div/div[3]/div[1]/div/div[2]/div/div/div/div[1]/div[2]/div[1]/div/span[1]'))

balance = driver.find_element_by_xpath('//*[@id="root"]/div/div[1]/div/div/div/div/div[1]/div/div/div/div[2]/div/div/div/div[3]/div[1]/div/div[2]/div/div/div/div[1]/div[2]/div[1]/div/span[1]')
print('Баланс: '+ balance.get_attribute('span'))
gb = WebDriverWait(driver, 20).until(EC.presence_of_element_located((By.XPATH, '//*[@id="root"]/div/div[1]/div/div/div/div/div[1]/div/div/div/div[2]/div/div/div/div[3]/div[1]/div/div[2]/div/div/div/div[1]/div[3]/div/div[3]/span/span/span[1]')))
print(' Остаток интернета: '+gb.getAttribute("innerHTML"))
call = WebDriverWait(driver, 20).until(EC.presence_of_element_located((By.XPATH, '//*[@id="root"]/div/div[1]/div/div/div/div/div[1]/div/div/div/div[2]/div/div/div/div[3]/div[1]/div/div[2]/div/div/div/div[1]/div[3]/div/div[2]/span/span/span[1]')))
print(' Остаток звонков:  '+call.get_attribute("span"))
sms = WebDriverWait(driver, 20).until(EC.presence_of_element_located((By.XPATH, '//*[@id="root"]/div/div[1]/div/div/div/div/div[1]/div/div/div/div[2]/div/div/div/div[3]/div[1]/div/div[2]/div/div/div/div[1]/div[3]/div/div[4]/span/span/span[1]')))

print(' Остаток смс: '+sms.get_attribute("value"))
#driver.quit()
